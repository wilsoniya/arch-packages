#!/usr/bin/env bash

# script called by openvpn-client during startup and teardown
# see: man openvpn

set -x
set -e

# $1 - cmd
# $1 - tap_dev
# $2 - tap_mtu
# $3 - link_mtu
# $4 - ifconfig_local_ip
# $5 - ifconfig_netmask
# $6 - [ init | restart ]

NETNS_NAME=torrents

case $script_type in
    up)
        ip link set dev "$1" up netns $NETNS_NAME mtu "$2"
        ip netns exec $NETNS_NAME ip addr add dev "$1" \
                "$4/${ifconfig_netmask:-30}" \
                ${ifconfig_broadcast:+broadcast "$ifconfig_broadcast"}

        if test -n "$ifconfig_ipv6_local"; then
            ip netns exec $NETNS_NAME ip addr add dev "$1" "$ifconfig_ipv6_local"/112
        fi
    ;;

    route-up)
        ip netns exec $NETNS_NAME ip route add default via "$route_vpn_gateway"
        if test -n "$ifconfig_ipv6_remote"; then
            ip netns exec $NETNS_NAME ip route add default via "$ifconfig_ipv6_remote"
        fi
    ;;

    down)
        echo "no-op"
    ;;
esac

#!/usr/bin/env bash

set -x

if [[ $UID != 0 ]]; then
    echo "This must be run as root."
    exit 1
fi

function iface_up() {
    ip netns add torrents

    ip netns exec torrents ip addr add 127.0.0.1/8 dev lo
    ip netns exec torrents ip link set lo up

    # Create linked virtual interfaces vpn0, vpn1: see man 4 veth.
    # This allows for connections into the netns from the global namespace
    # which enables things like administering the torrent daemon
    ip link add vpn0 type veth peer name vpn1
    ip link set vpn0 up
    # move vpn1 into the netns
    ip link set vpn1 netns torrents up

    ip addr add 192.168.2.1/24 dev vpn0
    ip netns exec torrents ip addr add 192.168.2.2/24 dev vpn1

    mkdir -p /etc/netns/torrents
    echo 'nameserver 1.1.1.1' > /etc/netns/torrents/resolv.conf
}

function iface_down() {
    rm -rf /etc/netns/torrents

    ip netns delete torrents
}


case "$1" in
    up)
        iface_up ;;
    down)
        iface_down ;;
    *)
        echo "Syntax: $0 up|down"
        exit 1
        ;;
esac
